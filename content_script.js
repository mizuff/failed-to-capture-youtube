console.log('content_script');
domtoimage.toBlob(document.getElementsById('movie_player'))
  .then(function (blob) {
    saveAs(blob, 'movie.png');
  })
  .catch(function (error) {
    console.error('oops, something went wrong!', error);
  });